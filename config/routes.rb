Rails.application.routes.draw do
  root 'posts#index'

  devise_for :users

  resources :users, only: [] do
    resources :posts, only: [:index]
  end

  resources :posts
  resources :comments
end
