module ApplicationHelper

  def created_at_format(resource, format=nil)
    if resource.respond_to?(:created_at)
      resource.created_at.strftime(format ? format : "%d.%m.%Y %H:%M")
    end
  end

end
