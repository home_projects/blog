module PostsHelper
  include ActsAsTaggableOn::TagsHelper

  def it_is_my_posts_list
    current_user && (params[:user_id].to_i == current_user.id)
  end
end
