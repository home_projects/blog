$ ->
  Blog.posts =
    switchPublished: (target, published) ->
      make_unpublished = target.prop('checked')
      published
        .prop('checked', !make_unpublished)
        .val(!make_unpublished)

    formSubmitDisabledByEnter: (form) ->
      form.on 'keydown', (e) ->
        code = if e.keyCode then e.keyCode else e.which
        if (code == 13)
          e.preventDefault()
          return false

    deleteTag: (el) ->
      el.closest('.tag').remove()

    addTag: (new_tag_input) ->
      new_tag_input.on 'keyup', (e) ->
        new_tag_val = new_tag_input.val()
        if new_tag_val.trim().length == 0
          return false

        code = if e.keyCode then e.keyCode else e.which

        if (code == 13)
          tag_template = $('.tag_template').clone()

          tag_template.find('input').removeClass('tag_template').attr('value', new_tag_val)
          tag_template.find('.visible-tag span').text(new_tag_val)
          tag_template.removeClass('tag_template')

          tags = $('#tags')
          tags.append(tag_template)
          new_tag_input.val('')

    bindEvents: ->
      this_ = @
      $('.new_post, .edit_post').each ->
        this_.formSubmitDisabledByEnter($(@))

      new_tag_input = $('#enter_tag')
      if new_tag_input.length > 0
        @addTag(new_tag_input)

    init: ->
      @bindEvents()

  Blog.posts.init()