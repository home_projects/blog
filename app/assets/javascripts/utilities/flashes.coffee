$ ->
  Blog.flashes =
    
    setHtmlType: (type) ->  
      # notice  - green (html_type = 'success' by default)
      # info    - blue
      # warning - yellow
      # alert, error - red

      html_type = 'success'
    
      if ( $.inArray(type, ['alert', 'error']) != -1 )
        html_type = 'danger'
      
    
      if ( $.inArray(type, ['warning', 'info']) != -1 )
        html_type = type
    
      return html_type

    flashHtml: (type, msg) ->  
      html_type = @setHtmlType(type)
      return '<div class="alert fade in alert-' + html_type + '"><button type="button" class="close" data-dismiss="alert">×</button>' + msg + '</div>'
    

    showMsg: (type, msg) ->
      flash_wrapper = $('#bootstrap_flashes')
      flash_wrapper.html('')
      flash_wrapper.append(Blog.flashes.flashHtml(type, msg))
      @hideMsg()

    hideMsg: ->
      alert = $('#bootstrap_flashes .alert')
      if alert.length > 0
        setTimeout ->
          alert.fadeOut 1000, ->
            alert.remove()
        , 5000

    init: ->
      @hideMsg()

  Blog.flashes.init()