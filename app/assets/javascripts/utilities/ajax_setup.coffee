$ ->
  # Default ajax errors handler
  $(document).ajaxError (event, jqxhr) ->
    if (jqxhr.status == 401)
      Blog.flashes.showMsg('info', "Please log in to perform this action.");
    else
      Blog.flashes.showMsg('error', "Something went wrong.");
