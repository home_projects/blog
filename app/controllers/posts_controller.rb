class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:edit, :update, :destroy]
  before_action :tag_cloud, only: [:index]
  before_action :post_tag_cloud, only: [:show]

  def index
    if params[:user_id].present?
      @posts = Post.where(user_id: params[:user_id].to_i)
    else
      @posts = params[:tag].present? ? Post.tagged_with(params[:tag]) : @posts = Post.all
      @posts = @posts.published
    end

    @posts = @posts.includes(:user).order('created_at desc').page params[:page]
  end

  def show
    if !@post.managed_by?(current_user) && !@post.published
      flash[:warning] = "You can't read unpublished post."
      redirect_to root_path
    end
    @comments = @post.comments.order('created_at asc')
    @comment = Comment.new(post: @post)
    @related_posts = @post.find_related_tags.published
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = current_user.posts.build(post_params)

    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      render :new
    end
  end

  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to user_posts_path(current_user), notice: 'Post was successfully destroyed.'
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      data = params.fetch(:post, {}).permit(:body, :published, :created_at, tag_list: [])
      data['tag_list']= data['tag_list'].reject(&:blank?).uniq.join(',')
      data
    end

    def check_access
      unless @post.managed_by?(current_user)
        flash[:warning] = 'You need permission to perform this action.'
        redirect_to root_path
      end
    end

    def post_tag_cloud
      @tags = @post.tag_counts_on(:tags)
    end

end
