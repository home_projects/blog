class CommentsController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:edit, :update, :destroy]
  before_action :check_before_edit, only: [:edit, :update, :destroy]


  def create
    @comment = Comment.new(comment_params)

    respond_to do |format|
      if @comment.save
        format.js {}
      else
        format.js {render :new}
      end
    end
  end

  def edit
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.js {}
      else
        format.js { render :edit }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment_id = @comment.id
    @comment.destroy
    respond_to do |format|
      format.js {}
    end
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.fetch(:comment, {}).permit(:body, :post_id, :user_id)
    end

  def check_access
    unless @comment.managed_by?(current_user)
      flash[:warning] = 'You need permission to perform this action.'
      redirect_to post_path @comment.post
    end
  end

  def check_before_edit
    unless @comment.editing?
      respond_to do |format|
        @msg = "You can't edit comment if it was created more than 15 minutes ago."
        format.html {
          flash[:warning] = @msg
          redirect_to @comment.post
        }
        format.js {render :time_is_over}
      end
    end
  end
end
