module CommonMethods
  extend ActiveSupport::Concern

  def managed_by?(user)
    user && (user.id == user_id)
  end
end