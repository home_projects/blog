class Post < ActiveRecord::Base
  include CommonMethods

  acts_as_taggable

  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :body, presence: true

  paginates_per 5

  scope :published, -> {where(published: true)}
end
