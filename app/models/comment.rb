class Comment < ActiveRecord::Base
  include CommonMethods

  belongs_to :user
  belongs_to :post

  validates :body, :post_id, :user_id, presence: true

  def editing?
    Time.now - created_at < 15.minutes
  end
end
