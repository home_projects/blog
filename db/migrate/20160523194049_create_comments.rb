class CreateComments < ActiveRecord::Migration
  def up
    create_table :comments do |t|
      t.text :body, null: false
      t.references :user, null: false, index: true
      t.references :post, null: false, index: true
      t.timestamps null: false
    end
  end

  def down
    drop_table :comments
  end
end
