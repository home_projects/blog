class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.text :body, null: false
      t.boolean :published, null: false, default: true, index: true
      t.references :user, null: false, index: true
      t.timestamps null: false
    end

    add_index :posts, :created_at
  end

  def down
    drop_table :posts
  end
end
