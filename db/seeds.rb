# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
['john@mail.com', 'andrew@mail.com'].each do |email|
  User.find_or_create_by(email: email) do |user|
    user.password = 11111111
    user.password_confirmation = 11111111
  end
end

User.all.limit(2).each do |user|
  unless user.posts.any?
    email_prefix = user.email.split('@').first.capitalize
    10.times do |i|
      user.posts.build(body: "#{email_prefix} post ##{i+1}", created_at: 1.hour.ago + i.minutes).save
    end
  end
end

tags = ['sport', 'programming', 'music', 'education', 'food', 'culture']

Post.all.limit(20).each do |post|
  start_range = rand(0..tags.length-1)
  post.tag_list = tags[start_range..start_range + 1]
  post.save
end
