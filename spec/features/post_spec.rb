include TestHelpers

feature 'Posts' do
  context 'Signed-in user' do

    let!(:user) {sign_in FactoryGirl.create(:user)}

    context 'Post CRUD' do
      scenario 'user can create post and post published: true by default' do
        visit new_post_path
        post_body = Faker::Lorem.paragraph
        fill_in 'post_body', with: post_body
        click_on 'Create Post'

        post = Post.find_by_body(post_body)
        expect(page).to have_content 'Post was successfully created'
        expect(post.present?).to be true
        expect(post.published).to be true
      end

      scenario 'user can create Unpublished post', js: true do
        Capybara.current_driver = :poltergeist

        visit new_post_path
        post_body = Faker::Lorem.paragraph
        fill_in 'post_body', with: post_body
        find('#without_publication').click
        click_on 'Create Post'

        post = Post.find_by_body(post_body)
        expect(page).to have_content 'Post was successfully created'
        expect(post.present?).to be true
        expect(post.published).to be false
      end

      scenario 'user can change Unpublished post to Published post', js: true do
        Capybara.current_driver = :poltergeist

        post = FactoryGirl.create(:post, :unpublished, user: user)
        visit edit_post_path(post)

        find('#post_published').click
        click_on 'Update Post'

        post = post.reload
        expect(page).to have_content 'Post was successfully updated'
        expect(post.present?).to be true
        expect(post.published).to be true
      end

      scenario 'user can create post with tags', js: true do
        Capybara.current_driver = :poltergeist

        visit new_post_path

        post_body = Faker::Lorem.paragraph
        post_tag_1 = 'ruby'
        post_tag_2 = 'angular'

        fill_in 'post_body', with: post_body
        fill_in 'enter_tag', with: post_tag_1
        find('#enter_tag').native.send_keys(:return)
        fill_in 'enter_tag', with: post_tag_2
        find('#enter_tag').native.send_keys(:return)

        click_on 'Create Post'

        post = Post.find_by_body(post_body)

        expect(page).to have_content 'Post was successfully created'
        expect(post.present?).to be true
        expect(post.tag_list.sort).to eq [post_tag_1, post_tag_2].sort
      end

      scenario 'user can update post' do
        Capybara.use_default_driver
        post = FactoryGirl.create(:post, user: user)

        visit edit_post_path(post)
        fill_in 'post_body', with: 'abc'
        click_on 'Update Post'

        expect(post.reload.body).to eq 'abc'
        expect(page).to have_content 'Post was successfully updated'
      end

      scenario 'user can destroy post' do
        post = FactoryGirl.create(:post, user: user)

        visit user_posts_path(user)

        expect(post.in? user.posts).to eq true

        find("#delete-#{post.id}").click

        expect(page).to have_content 'Post was successfully destroyed'
        expect(post.in? user.posts).to eq false
      end

      scenario 'no Unpublished posts on "All Posts" page' do
        unpublished_post = FactoryGirl.create(:post, :unpublished, user: user)
        post = FactoryGirl.create(:post, user: user)
        visit posts_path

        expect(page.all("#post-#{post.id}").length).to eq(1)
        expect(page.all("#post-#{unpublished_post.id}").length).to eq(0)
      end

      scenario 'user can see his Unpublished post' do
        unpublished_post = FactoryGirl.create(:post, :unpublished, user: user)
        post = FactoryGirl.create(:post, user: user)
        visit user_posts_path(user)

        expect(page.all("#post-#{post.id}").length).to eq(1)
        expect(page.all("#post-#{unpublished_post.id}").length).to eq(1)
      end

      scenario 'user can see post through action Show' do
        post = FactoryGirl.create(:post, user: user)

        visit post_path(post)
        expect(page.all("#post-#{user.posts.first.id}").length).to eq(1)
      end
    end

    context 'Tags' do
      let!(:post1) {FactoryGirl.create(:post, :with_tags, tag_list: 'tag1')}
      let!(:post2) {FactoryGirl.create(:post, :with_tags, tag_list: 'tag1, tag2')}
      let!(:post3) {FactoryGirl.create(:post, :with_tags, tag_list: 'tag1, tag2, tag3')}

      scenario 'can see posts by tag' do
        visit posts_path

        page.find('#tag_cloud').find('a', text: 'tag1').click

        expect(page).to have_css("#post-#{post1.id}")
        expect(page).to have_css("#post-#{post2.id}")
        expect(page).to have_css("#post-#{post3.id}")

        page.find('#tag_cloud').find('a', text: 'tag2').click

        expect(page).to have_css("#post-#{post2.id}")
        expect(page).to have_css("#post-#{post3.id}")

        page.find('#tag_cloud').find('a', text: 'tag3').click

        expect(page).to have_css("#post-#{post3.id}")
      end

      scenario 'can see related posts' do
        visit post_path(post1)

        expect(page).to have_css("a[id=related-post-#{post2.id}]")
        expect(page).to have_css("a[id=related-post-#{post3.id}]")
      end
    end

    context 'Posts related with different users' do
      let!(:user_post) {FactoryGirl.create(:post, user: user)}
      let!(:post2) {FactoryGirl.create(:post)}
      let!(:post3) {FactoryGirl.create(:post)}

      scenario 'user can see only his own posts on "My Posts" page' do
        visit user_posts_path(user)

        expect(page).to have_css("#post-#{user_post.id}")

        expect(page).to have_no_css("#post-#{post2.id}")
        expect(page).to have_no_css("#post-#{post3.id}")
      end

      scenario 'user can see all posts on "All Posts" page' do
        visit posts_path

        expect(page).to have_css("#post-#{user_post.id}")
        expect(page).to have_css("#post-#{post2.id}")
        expect(page).to have_css("#post-#{post3.id}")
      end
    end

  end

  context 'User without session' do

    context 'Posts' do
      let!(:post1) {FactoryGirl.create(:post)}
      let!(:post2) {FactoryGirl.create(:post, :with_comments)}

      scenario 'user can see all posts on "All Posts" page' do
        visit posts_path

        expect(page).to have_css("#post-#{post1.id}")
        expect(page).to have_css("#post-#{post2.id}")
      end

      scenario 'user cant see post manage links on All Posts page' do
        visit posts_path

        expect(page).to have_no_css('.edit-link')
        expect(page).to have_no_css('.destroy-link')
      end

      scenario 'user can see post with comments' do
        visit post_path(post2)

        expect(page).to have_css("#post-#{post2.id}")
        post2.comments.each do |comment|
          expect(page).to have_css("#comment-#{comment.id}")
        end
      end

      scenario 'user cant see post comments manage links on post page' do
        visit post_path(post2)

        expect(page).to have_no_css('.manage_links')
      end

      scenario 'user cant add comment', js: true do
        Capybara.current_driver = :poltergeist

        visit post_path(post2)

        fill_in 'comment_body', with: 'abc'
        click_on 'Create Comment'

        sleep 1

        expect(page).to have_content('Please log in to perform this action')
      end

      scenario 'user cant create post' do
        visit new_post_path

        expect(page).to have_content('You need to sign in or sign up before continuing')
      end
    end

  end

end