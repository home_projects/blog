include TestHelpers

feature 'Devise' do

  context 'Any user' do
    scenario 'can be registered' do
      visit root_path

      find('#sign-up-link').click

      within("#new_user") do
        fill_in 'user_email', :with => 'user@example.com'
        fill_in 'user_password', :with => '12345678'
        fill_in 'user_password_confirmation', :with => '12345678'
      end
      click_button 'Sign up'
      expect(User.last.email).to eq 'user@example.com'
      expect(page).to have_content 'Welcome! You have signed up successfully'
    end
  end

  context 'Existing user' do
    scenario 'can be logged in' do
      user = FactoryGirl.create(:user)

      visit root_path

      find('#log-in-link').click

      within("#new_user") do
        fill_in 'user_email', :with => user.email
        fill_in 'user_password', :with => '11111111'
      end

      click_button 'Log in'
      expect(page).to have_content 'Signed in successfully'
    end
  end

  context 'Current user' do
    scenario 'can sign out' do
      user = FactoryGirl.create(:user)
      sign_in user

      visit root_path

      find('#sign-out-link').click

      expect(page).to have_content 'Signed out successfully'
    end
  end

end




