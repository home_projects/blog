include TestHelpers

feature 'Comment' do
  let!(:user) {sign_in FactoryGirl.create(:user)}

  context 'Signed-in user' do

    scenario 'user can add comment to post', js: true do
      Capybara.current_driver = :poltergeist

      post = FactoryGirl.create(:post, user: user)

      visit post_path(post)

      comment_body = Faker::Lorem.paragraph
      fill_in 'comment_body', with: comment_body
      click_on 'Create Comment'

      sleep 1

      expect(page.all("#comment-#{post.id}").length).to eq 1
      expect(post.comments.where(body: comment_body).length).to eq 1
    end

  end

  context 'Post with comments' do
    scenario 'user can see manage links for his own comment but cant see other' do
      post_with_user_comments = FactoryGirl.create(:post, :with_user_comments, user: user )

      visit post_path(post_with_user_comments)

      user_comment = post_with_user_comments.comments.where(user: user).first
      another_users_comments = post_with_user_comments.comments.where.not(user: user)

      expect(page).to have_css("#comment-wrapper-#{user_comment.id} .manage_links")
      another_users_comments.each do |comment|
        expect(page).to have_no_css("#comment-wrapper-#{comment.id} .manage_links")
      end
    end

    scenario 'user can edit his own comment' do

      post_with_user_comments = FactoryGirl.create(:post, :with_user_comments, user: user )

      visit post_path(post_with_user_comments)

      user_comment = post_with_user_comments.comments.where(user: user).first

      page.find(("#comment-wrapper-#{user_comment.id} .manage_links .edit-comment-link")).click

      new_comment_body = 'abc'
      fill_in 'comment_body', with: new_comment_body
      click_on 'Update Comment'

      expect(user_comment.reload.body).to eq new_comment_body
    end

    scenario 'user can destroy his own comment', js: true do
      Capybara.current_driver = :poltergeist
      post_with_user_comments = FactoryGirl.create(:post, :with_user_comments, user: user )

      visit post_path(post_with_user_comments)

      user_comment = post_with_user_comments.comments.where(user: user).first

      page.find(("#comment-wrapper-#{user_comment.id} .manage_links .destroy-comment-link")).click
      # page.driver.browser.switch_to.alert.accept

      sleep 1

      expect(page).to have_no_css("#comment-wrapper-#{user_comment.id}")
      expect(Comment.find_by_id(user_comment.id)).to be nil
    end

    scenario 'user cant see his own comment manage links 15 minutes ago after creating', js: true do
      Capybara.current_driver = :poltergeist
      post_with_user_comments = FactoryGirl.create(:post, :with_user_comments, user: user )
      user_comment = post_with_user_comments.comments.where(user: user).first
      user_comment.update(created_at: Time.now - 15.minutes)

      visit post_path(post_with_user_comments)

      expect(page).to have_no_css("#comment-wrapper-#{user_comment.id} .manage_links")
    end

    scenario 'user can manage his own comment 14 minutes ago after creating' do
      post_with_user_comments = FactoryGirl.create(:post, :with_user_comments, user: user )
      user_comment = post_with_user_comments.comments.where(user: user).first

      user_comment.update(created_at: Time.now - 14.minutes)
      expect(user_comment.editing?).to be true

      visit edit_comment_path(user_comment)

      new_comment_body = 'abc'
      fill_in 'comment_body', with: new_comment_body
      click_on 'Update Comment'

      expect(user_comment.reload.body).to eq new_comment_body
    end

    scenario 'user cant manage his own comment 15 minutes ago after creating' do
      post_with_user_comments = FactoryGirl.create(:post, :with_user_comments, user: user )
      user_comment = post_with_user_comments.comments.where(user: user).first

      user_comment.update(created_at: Time.now - 15.minutes)
      expect(user_comment.editing?).to be false

      visit edit_comment_path(user_comment)

      expect(page).to have_content("You can't edit comment if it was created more than 15 minutes ago")
    end
  end

end