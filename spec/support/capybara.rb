RSpec.configure do |config|
  require 'capybara/poltergeist'

  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, timeout: 60)
  end

  Capybara.javascript_driver = :poltergeist

  Capybara.default_max_wait_time = 5
end