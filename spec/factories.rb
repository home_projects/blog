FactoryGirl.define do

  factory :user do
    sequence :email do |n|
      "user#{n}@domain.com"
    end
    password '11111111'
    password_confirmation '11111111'
  end

  factory :comment do
    body 'Coment_body'
    user
    post
  end

  factory :post do
    body 'Post_body'
    user

    trait :published do
      published true
    end

    trait :unpublished do
      published false
    end

    trait :with_comments do
      after :create do |post|
        FactoryGirl.create_list :comment, 3, post: post
      end
    end

    trait :with_user_comments do
      after(:create) do |post, evaluator|
        FactoryGirl.create_list(:comment, 1, user_id: evaluator.user.id, post: post)
        FactoryGirl.create_list(:comment, 2, post: post)
      end
    end

    trait :with_tags do
      after(:create) do |post, evaluator|
        post.tag_list = evaluator.tag_list
        post.save
      end
    end
  end

end


