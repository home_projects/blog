RSpec.describe Comment, type: :model do
  it "can be created" do
    Comment.create(body: 'comment_body', user_id: 1, post_id: 1)
    expect(Comment.find_by_body('comment_body').present?).to be true
  end

  it "comment belongs_to user and post" do
    user = FactoryGirl.create(:user)
    post = FactoryGirl.create(:post)
    comment = FactoryGirl.create(:comment, user: user, post: post)

    expect(comment.post).to eq post
    expect(comment.user).to eq user
  end
end
