RSpec.describe Post, :type => :model do
  it "can be created" do
    Post.create(body: 'post_body', user_id: 1)
    expect(Post.find_by_body('post_body').present?).to be true
  end

  it "created post is published by default" do
    Post.create(body: 'post_body', user_id: 1)
    expect(Post.find_by_body('post_body').published).to be true
  end

  it "post belongs_to user" do
    user = FactoryGirl.create(:user)
    post = FactoryGirl.create(:post, body: 'post_body', user: user)
    expect(Post.find_by_body('post_body').user).to eq user
  end

  it "post has_many comments" do
    post = FactoryGirl.create(:post)
    comment = FactoryGirl.create(:comment, post: post)
    expect(comment.in? post.comments).to be true
  end

  it "post has scope published that returns posts with published: true" do
    FactoryGirl.create(:post, :published)
    FactoryGirl.create(:post, :unpublished)

    expect(Post.published.count).to eq 1
    expect(Post.published.pluck(:published).uniq).to eq [true]
  end
end